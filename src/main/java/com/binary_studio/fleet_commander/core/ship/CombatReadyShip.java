package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class CombatReadyShip implements CombatReadyVessel {

	private final String name;

	private final PositiveInteger capacitorRechargeRate;

	private final PositiveInteger speed;

	private final PositiveInteger size;

	private final DefenciveSubsystem defenciveSubsystem;

	private final AttackSubsystem attackSubsystem;

	private final PositiveInteger initialShieldHP;

	private final PositiveInteger initialHullHP;

	private final PositiveInteger initialCapasitorAmount;

	private PositiveInteger shieldHP;

	private PositiveInteger hullHP;

	private PositiveInteger capacitorAmount;

	public CombatReadyShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate, PositiveInteger speed,
			PositiveInteger size, DefenciveSubsystem defenciveSubsystem, AttackSubsystem attackSubsystem) {

		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.capacitorAmount = capacitorAmount;
		this.capacitorRechargeRate = capacitorRechargeRate;
		this.speed = speed;
		this.size = size;
		this.defenciveSubsystem = defenciveSubsystem;
		this.attackSubsystem = attackSubsystem;

		this.initialHullHP = hullHP;
		this.initialShieldHP = shieldHP;
		this.initialCapasitorAmount = capacitorAmount;
	}

	@Override
	public void endTurn() {
		this.capacitorAmount = this.capacitorRechargeRate.plus(this.capacitorAmount)
				.value() <= this.initialCapasitorAmount.value() ? this.capacitorRechargeRate.plus(this.capacitorAmount)
						: this.initialCapasitorAmount;
	}

	@Override
	public void startTurn() {
		// Must be empty

	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public PositiveInteger getSize() {
		return this.size;
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.speed;
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		PositiveInteger damage = this.attackSubsystem.attack(target);

		if (this.capacitorAmount.value() >= this.attackSubsystem.getCapacitorConsumption().value()) {
			this.capacitorAmount = this.capacitorAmount.minus(this.attackSubsystem.getCapacitorConsumption());
			return Optional.of(new AttackAction(damage, this, target, this.attackSubsystem));
		}

		return Optional.empty();
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		AttackAction reducedAttackAction = this.defenciveSubsystem.reduceDamage(attack);
		PositiveInteger restOfDamage;

		if (this.shieldHP.value() > reducedAttackAction.damage.value()) {
			this.shieldHP = this.shieldHP.minus(reducedAttackAction.damage);
			restOfDamage = PositiveInteger.of(0);
		}
		else {
			this.shieldHP = PositiveInteger.of(0);
			restOfDamage = reducedAttackAction.damage.minus(this.shieldHP);
		}

		if (this.hullHP.value() > restOfDamage.value()) {
			this.hullHP = this.hullHP.minus(restOfDamage);
		}
		else {
			this.hullHP = PositiveInteger.of(0);
		}

		return this.shieldHP.value() == 0 && this.hullHP.value() == 0 ? new AttackResult.Destroyed()
				: new AttackResult.DamageRecived(reducedAttackAction.weapon, reducedAttackAction.damage,
						reducedAttackAction.target);
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		if (this.capacitorAmount.value() >= this.defenciveSubsystem.getCapacitorConsumption().value()) {
			RegenerateAction regenerateAction = this.defenciveSubsystem.regenerate();
			PositiveInteger hullHpToRegenerate = this.hullHP;
			PositiveInteger shieldHpToRegenerate = this.shieldHP;

			this.hullHP = regenerateAction.hullHPRegenerated.plus(this.hullHP).value() > this.initialHullHP.value()
					? this.initialHullHP : regenerateAction.hullHPRegenerated.plus(this.hullHP);
			this.shieldHP = regenerateAction.shieldHPRegenerated.plus(this.shieldHP).value() > this.initialShieldHP
					.value() ? this.initialShieldHP : regenerateAction.shieldHPRegenerated.plus(this.shieldHP);

			this.capacitorAmount = this.capacitorAmount.minus(this.defenciveSubsystem.getCapacitorConsumption());

			return Optional.of(new RegenerateAction(this.shieldHP.minus(shieldHpToRegenerate),
					this.hullHP.minus(hullHpToRegenerate)));
		}

		return Optional.empty();
	}

}
