package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DockedShip implements ModularVessel {

	private final String name;

	private final PositiveInteger shieldHP;

	private final PositiveInteger hullHP;

	private final PositiveInteger capacitorAmount;

	private final PositiveInteger capacitorRechargeRate;

	private final PositiveInteger speed;

	private final PositiveInteger size;

	private PositiveInteger powerGridOutput;

	private DefenciveSubsystem defenciveSubsystem = null;

	private AttackSubsystem attackSubsystem = null;

	private DockedShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger powerGridOutput,
			PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate, PositiveInteger speed,
			PositiveInteger size) {

		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.powerGridOutput = powerGridOutput;
		this.capacitorAmount = capacitorAmount;
		this.capacitorRechargeRate = capacitorRechargeRate;
		this.speed = speed;
		this.size = size;
	}

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powerGridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {
		return new DockedShip(name, shieldHP, hullHP, powerGridOutput, capacitorAmount, capacitorRechargeRate, speed,
				size);
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem != null && this.powerGridOutput.value() < subsystem.getPowerGridConsumption().value()) {
			throw new InsufficientPowergridException(
					subsystem.getPowerGridConsumption().value() - this.powerGridOutput.value());
		}

		this.attackSubsystem = subsystem;

		if (subsystem != null) {
			this.powerGridOutput = this.powerGridOutput.minus(subsystem.getPowerGridConsumption());
		}
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem != null && this.powerGridOutput.value() < subsystem.getPowerGridConsumption().value()) {
			throw new InsufficientPowergridException(
					subsystem.getPowerGridConsumption().value() - this.powerGridOutput.value());
		}

		this.defenciveSubsystem = subsystem;

		if (subsystem != null) {
			this.powerGridOutput = this.powerGridOutput.minus(subsystem.getPowerGridConsumption());
		}
	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		if (this.defenciveSubsystem == null && this.attackSubsystem == null) {
			throw NotAllSubsystemsFitted.bothMissing();
		}
		if (this.defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.defenciveMissing();
		}
		if (this.attackSubsystem == null) {
			throw NotAllSubsystemsFitted.attackMissing();
		}

		return new CombatReadyShip(this.name, this.shieldHP, this.hullHP, this.capacitorAmount,
				this.capacitorRechargeRate, this.speed, this.size, this.defenciveSubsystem, this.attackSubsystem);
	}

}
