package com.binary_studio.tree_max_depth;

import java.util.Stack;

public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {
	}

	public static Integer calculateMaxDepth(Department rootDepartment) {
		if (rootDepartment == null) {
			return 0;
		}

		int maxDepth = 0;
		Stack<Department> path = new Stack<>();
		Stack<Department> departments = new Stack<>();

		departments.push(rootDepartment);
		while (!departments.empty()) {
			rootDepartment = departments.peek();
			if (!path.empty() && rootDepartment == path.peek()) {
				if (path.size() > maxDepth) {
					maxDepth = path.size();
				}
				path.pop();
				departments.pop();
			}
			else {
				path.push(rootDepartment);
				rootDepartment.subDepartments.forEach(sub -> {
					if (sub != null) {
						departments.push(sub);
					}
				});
			}
		}

		return maxDepth;
	}

}
