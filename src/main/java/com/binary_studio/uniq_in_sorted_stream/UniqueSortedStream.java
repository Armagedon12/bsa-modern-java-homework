package com.binary_studio.uniq_in_sorted_stream;

import java.util.function.Predicate;
import java.util.stream.Stream;

public final class UniqueSortedStream {

	private UniqueSortedStream() {
	}

	public static <T> Stream<Row<T>> uniqueRowsSortedByPK(Stream<Row<T>> stream) {
		return stream.filter(new NoRepeatFilter<>());
	}

	// Note for tutor:
	// This solution won`t work for parallel mode, but it was not added to requirements :)
	private static class NoRepeatFilter<T extends Row<?>> implements Predicate<T> {

		private Long prevValue;

		@Override
		public boolean test(T value) {
			if (value.getPrimaryId().equals(this.prevValue)) {
				return false;
			}
			this.prevValue = value.getPrimaryId();
			return true;
		}

	}

}
